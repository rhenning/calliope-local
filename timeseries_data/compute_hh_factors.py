# file to compute factors for relative household sizes, based on their electricity demand profiles
import pandas as pd

# read electricity file (assuming it's in the same folder as this script)
elec_file = 'electricity-demand.csv'
el_df = pd.read_csv(elec_file, sep=',', header=0, index_col=0)

factors_df = pd.DataFrame(index=el_df.columns.values,
                          data=[-sum(el_df[col]) for col in el_df],
                          columns=['total consumption'])
factors_df.index.name = 'household'

average_yearly_consumption = sum(factors_df['total consumption'])/len(factors_df.index)
factors_df['factor'] = factors_df['total consumption'].div(average_yearly_consumption)
factors_df.to_csv('household_factors.csv', sep=';')
print(factors_df)


