from pathlib import Path
from lxml import etree
import ast
import os
import pandas as pd
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 33})
# plt.rcParams['figure.figsize'] = [16, 14]
import seaborn as sns
import seaborn.objects as so
import plotly.express as px
import numpy as np

np.set_printoptions(formatter={'float_kind': '{:f}'.format})

# number of households:
n_hh = 25
# fixed network tariff:
tariff_fixed = 250

# establish file path of root directory of the repository:
reporoot_dir = Path(__file__).resolve().parent

result_dir = reporoot_dir / 'Results'


# html_file = result_dir / 'carbon_budget_40_elec_rtp_x2_network_cap_2023_12_07_22-53.html'
# parser = etree.HTMLParser()
# tree = etree.parse(html_file, parser)
# for child in tree.getroot():
#     if child.tag == 'body':
#         for node in child:
#             for sub_node in node:
#                 if sub_node.get('id') == 'top':
#                     for sub_top_node in sub_node:
#                         for sub_sub_node in sub_top_node:
#                             if sub_sub_node.tag == 'script':
#                                 text_list = etree.tostring(sub_sub_node, pretty_print=True).split(b'\n')
#                                 data = text_list[8].strip().rstrip(b',&#13;').split(b'"legendgroup"')
#                                 found_gas = 0
#                                 found_elec = 0
#                                 for entry in data:
#                                     # print(str(entry)[:200])
#                                     if entry.find(b'solar_pv') > -1 & entry.find(b'export') < 0:
#                                         list_string = entry.split(b'"y":')[1].lstrip().rstrip(b'}, {').decode(
#                                                     'ascii')
#                                         pvgen_list = ast.literal_eval(list_string)
#                                         found_pvgen = 1
#                                         print(str(entry)[:200])
#                                     if entry.find(b'export') > -1:
#                                         list_string = entry.split(b'"y":')[1].lstrip().rstrip(b'}, {').decode(
#                                             'ascii')
#                                         pvgen_list = ast.literal_eval(list_string)
#                                         found_pvgen = 1
#                                         print(str(entry)[:200])
#                                     # if not found_gas and entry.find(b'supply_gas') > -1:
#                                     #     list_string = entry.split(b'"y":')[1].lstrip().rstrip(b'}, {').decode(
#                                     #         'ascii')
#                                     #     gas_list = ast.literal_eval(list_string)
#                                     #     found_gas = 1
#                                     # if not found_elec and entry.find(b'supply_grid_power') > -1:
#                                     #     list_string = entry.split(b'"y":')[1].lstrip().rstrip(b'}, {').decode(
#                                     #         'ascii')
#                                     #     elec_list = ast.literal_eval(list_string)
#                                     #     found_elec = 1
#                                     # if found_gas and found_elec:
#                                     #     return gas_list, elec_list
#                                         # return data_list
#                                         # print(data_list)
#                                         # break
#                                 break

def get_scenario_elements(scenario_name):
    if scenario_name.find('no_investments') > -1:
        constraint = 'No inv.'
        price_scen = scenario_name[15:].lstrip('_')
    else:
        constraint = ('Carb' + scenario_name[14:17].strip('_')).replace('Carb100', 'No constr.')
        price_scen = scenario_name[17:].lstrip('_')
    price_scen = price_scen \
        .replace('elec_rtp_net_cap', 'RTP +capacity') \
        .replace('elec_rtp_net_fixed', 'RTP +fixed') \
        .replace('net_elec_flat', 'kWh Flat') \
        .replace('net_elec_tou', 'kWh ToU')
    return constraint, price_scen


def get_network_data(html_file):
    parser = etree.HTMLParser()
    tree = etree.parse(html_file, parser)
    for child in tree.getroot():
        if child.tag == 'body':
            for node in child:
                for sub_node in node:
                    if sub_node.get('id') == 'top':
                        for sub_top_node in sub_node:
                            for sub_sub_node in sub_top_node:
                                if sub_sub_node.tag == 'script':
                                    text_list = etree.tostring(sub_sub_node, pretty_print=True).split(b'\n')
                                    data = text_list[8].strip().rstrip(b',&#13;').split(b'"legendgroup"')
                                    gas_list = []
                                    elec_list = []
                                    pvgen_list = []
                                    exp_list = []
                                    found_gas = 0
                                    found_elec = 0
                                    found_pvggen = 0
                                    for entry in data:
                                        if not found_gas and entry.find(b'supply_gas') > -1:
                                            list_string = entry.split(b'"y":')[1].lstrip().rstrip(b'}, {').decode(
                                                'ascii')
                                            gas_list = ast.literal_eval(list_string)
                                            found_gas = 1
                                        if not found_elec and entry.find(b'supply_grid_power') > -1:
                                            list_string = entry.split(b'"y":')[1].lstrip().rstrip(b'}, {').decode(
                                                'ascii')
                                            elec_list = ast.literal_eval(list_string)
                                            found_elec = 1
                                        if not str(html_file).find('no_investments') > -1:
                                            if not found_pvggen and entry.find(b'solar_pv') > -1 and entry.find(
                                                    b'export') < 0:
                                                list_string = entry.split(b'"y":')[1].lstrip().rstrip(b'}, {').decode(
                                                    'ascii')
                                                pvgen_list = ast.literal_eval(list_string)
                                                found_pvggen = 1
                                            if entry.find(b'demand_export_electricity') > -1:
                                                list_string = entry.split(b'"y":')[1].lstrip().rstrip(b'}, {').decode(
                                                    'ascii')
                                                exp_list = ast.literal_eval(list_string)
                                    return gas_list, elec_list, pvgen_list, exp_list


subfolders_paths = (f.path for f in os.scandir(result_dir) if f.is_dir())
html_filepaths = (os.path.join(result_dir, f) for f in os.listdir(result_dir) if f.endswith('.html'))
network_data = []
resource_data = []
for html_filepath in html_filepaths:
    scenario_name = str(html_filepath).split('\\')[-1].replace('.html', '')
    if (scenario_name.find('_x4') > -1) or scenario_name.find('no_investments') > -1:
        scenario_name = scenario_name[:-17].replace('network', 'net').replace('_x2', '').replace('_x4', ''). \
            replace('_5_', '_05_').replace('_1_', '_01_').replace('_2_', '_02_')
        print(scenario_name)
        constraint, price_scen = get_scenario_elements(scenario_name)
        gas_list, elec_list, pvgen_list, exp_list = get_network_data(html_filepath)
        gas_array = np.array(gas_list)
        elec_array = np.array(elec_list)
        if not pvgen_list:
            pvgen_array = np.zeros_like(elec_array)
        else:
            pvgen_array = np.array(pvgen_list)
        if not exp_list:
            exp_array = np.zeros_like(elec_array)
        else:
            exp_array = np.array(exp_list)
        elec_net = np.subtract(elec_array, exp_array)
        elec_feed_peak = - np.max(exp_array)
        elec_feed_avg = - np.average(exp_array)

        pvgen_sum = np.sum(pvgen_array)
        exp_sum = - np.sum(exp_array)
        gas_peak = np.max(gas_array)
        gas_avg = np.average(gas_array)
        gas_sum = np.sum(gas_array)
        elec_peak = np.max(elec_array)
        elec_avg = np.average(elec_array)
        if price_scen == 'RTP +capacity':
            elec_feed_peak = max(elec_feed_peak, -elec_peak)
            # limit export values to max capacity
            elec_feed_avg = np.average(np.clip(exp_array, elec_feed_peak, 0))
        elec_grid_sum = np.sum(elec_array)
        elec_cons_sum = pvgen_sum - exp_sum + elec_grid_sum
        resource_dict = {'El. (grid)': np.sum(elec_array), 'El. total': elec_cons_sum,
                         'Gas': gas_sum,
                         'PV gen.': pvgen_sum, 'PV export': exp_sum}
        network_dict = {'Gas peak': gas_peak, 'Gas avg': gas_avg,
                        'Load peak': elec_peak, 'Load avg': elec_avg,
                        'Feed-in peak': elec_feed_peak, 'Feed-in avg': elec_feed_avg, 'PV gen.': exp_sum/1000}
        for indicator, value in network_dict.items():
            network_data.append({'Price scenario': price_scen, 'Constraint': constraint,
                                 'Indicator': indicator, 'Value': value})
        for carrier, value in resource_dict.items():
            resource_data.append({'Price scenario': price_scen, 'Constraint': constraint,
                                  'Carrier': carrier, 'Total use': value})

network_df = (
    pd.DataFrame(network_data)
        .sort_values(['Constraint', 'Price scenario'], ascending=False)
)

techs_df = pd.DataFrame(columns=['Price scenario', 'Constraint', 'locs', 'techs', 'energy_cap'])
costs_data = []
for scenario_path in subfolders_paths:
    # get scenario as the last entry name from file path
    scenario_name = scenario_path.split('\\')[-1].replace('_csv', '')
    # print(scenario_name)
    if (scenario_name.find('_x4') > -1) or scenario_name.find('no_investments') > -1:

        # delete date from scenario name
        scenario_name = scenario_name[:-17]
        scenario_name = scenario_name.replace('network', 'net').replace('_x2', '').replace('_x4', ''). \
            replace('_5_', '_05_').replace('_1_', '_01_').replace('_2_', '_02_')
        print(scenario_name)
        constraint, price_scen = get_scenario_elements(scenario_name)
        # find files
        total_costs_file = Path(scenario_path) / 'results_cost.csv'
        inv_costs_file = Path(scenario_path) / 'results_cost_investment.csv'
        capacities_file = Path(scenario_path) / 'results_energy_cap.csv'
        # convert to pd dataframes
        cap_df = pd.read_csv(capacities_file, sep=',', header=0)

        total_costs_df = pd.read_csv(total_costs_file, sep=',', header=0)
        inv_costs_df = pd.read_csv(inv_costs_file, sep=',', header=0)
        # compute summary variables
        network_peak = float(cap_df.loc[(cap_df['locs'] == 'EG') &
                                        (cap_df['techs'] == 'supply_grid_power')]['energy_cap'].iloc[0])
        total_cost = total_costs_df.loc[(total_costs_df['costs'] == 'monetary')]['cost'].sum()

        inv_cost = inv_costs_df.loc[(inv_costs_df['costs'] == 'monetary') &
                                    (inv_costs_df['locs'] != 'EG')]['cost_investment'].sum()
        net_cap_cost = inv_costs_df.loc[(inv_costs_df['costs'] == 'monetary') &
                                        (inv_costs_df['techs'] == 'network_connection')]['cost_investment'].sum()
        # subtract network capacity cost from investment cost, as we count it towards variable costs:
        inv_cost -= net_cap_cost
        var_cost = total_cost - inv_cost
        if scenario_name.find('fixed') > -1:
            # add fixed network tariff to variable and total costs
            total_cost += n_hh * tariff_fixed
            var_cost += n_hh * tariff_fixed
        total_co2 = total_costs_df.loc[total_costs_df['costs'] == 'co2']['cost'].sum()
        locs = np.delete(total_costs_df['locs'].unique(), -1)
        loc_costs = np.array([total_costs_df.loc[total_costs_df['locs'] == loc, 'cost'].sum() for loc in locs])
        cost_spread = np.std(loc_costs) / np.average(loc_costs)
        total_diesel = total_costs_df.loc[(total_costs_df['costs'] == 'monetary') &
                                          (total_costs_df['techs'] == 'supply_diesel')]['cost'].sum() * 5
        resource_data.append({'Price scenario': price_scen, 'Constraint': constraint,
                              'Carrier': 'Diesel', 'Total use': total_diesel})
        if not scenario_name.find('no_investments') > -1:
            cap_df['Constraint'] = constraint
            cap_df['Price scenario'] = price_scen
            techs_df = pd.concat([techs_df, cap_df])
        costs = {'Variable costs': var_cost, 'Capital costs': inv_cost, 'Total cost': total_cost,
                 'Cost spread': cost_spread, 'Carbon emissions': total_co2, 'Diesel': total_diesel}
        for cost_type, cost in costs.items():
            costs_data.append({'Price scenario': price_scen, 'Constraint': constraint,
                               'Cost type': cost_type, 'Cost': cost})
    else:
        continue

resource_df = pd.DataFrame(resource_data)
# print(resource_df.to_string())
# print(network_df)
cost_df = (
    pd.DataFrame(costs_data)
        .sort_values(['Constraint', 'Price scenario'], ascending=False)
)

techs_df['techs'] = techs_df['techs'].replace({'air_source_hp': 'Air source HP',
                                               'ground_source_hp': ' Ground source HP',
                                               'renovation_improved': 'Improved renovation',
                                               'renovation_existing': 'Existing renovation',
                                               'renovation_enhanced': 'Enhanced renovation',
                                               'store_electricity': 'Battery', 'solar_pv': ' Solar PV',
                                               'electric_hob': ' Electric hob',
                                               'electric_boiler': 'Electric boiler'})
techs_df['Price scenario'] = techs_df['Price scenario'].replace({'elec_rtp_net_cap': 'RTP +capacity',
                                                                 'elec_rtp_net_fixed': 'RTP +fixed',
                                                                 'net_elec_flat': 'kWh Flat',
                                                                 'net_elec_tou': 'kWh ToU'})


def scenario_bar_charts(co2_df, rel_prices, variable):
    sns.set_theme(style="whitegrid")
    sns.set(font_scale=3)
    # cmap = clr.get_cmap('viridis')
    # scenarios = list(scenario_dict.keys())
    # scenarios.remove('_no_investments')
    # values = [100*float(scenario_dict[s][variable])/scenario_dict['_no_investments'][variable] for s in scenarios]
    hue_order = ['kWh ToU', 'kWh Flat', 'RTP +fixed', 'RTP +capacity', 'No inv.']
    p = sns.barplot(co2_df, x='Price scenario', y='Cost', hue='Price scenario', hue_order=hue_order,
                    palette="dark", alpha=.6)
    for i, price_scen in enumerate(hue_order[:4]):
        p.text(price_scen, 126500, rel_prices[i], ha="center")
    plt.xticks(rotation=0)
    # plt.tight_layout()
    plt.gca().yaxis.grid(True)
    plt.ylabel('Carbon emissions [kg CO2]')
    plt.xlabel('')
    plt.ylim(bottom=0.5)
    plt.show()


co2_abs = cost_df.loc[cost_df['Cost type'].isin(['Carbon emissions']) & (
        cost_df['Constraint'].isin(['No constr.'])
        |
        (cost_df['Constraint'].isin(['No inv.']) & (cost_df['Price scenario'] == 'kWh Flat'))
)]
co2_abs.loc[co2_abs['Constraint'] == 'No inv.', 'Price scenario'] = 'No inv.'
co2_rel = co2_abs.loc[co2_abs['Constraint'] == 'No constr.']
co2_rel['Cost'] = 100.0 * co2_rel['Cost'].divide(co2_abs.iloc[0, 3])
rel_prices = [("%.1f" % x) + ' %' for x in co2_rel['Cost'].tolist()]


# co2_rel['Cost'] = str(100.0*co2_rel['Cost'].divide(co2_abs.iloc[0,3])) + ' %'
# scenario_bar_charts(co2_abs, rel_prices, 'co2')

def capacities_scatter(tech_cap_df: pd.DataFrame, title):
    marker_dict = {'kWh ToU': 'x', 'kWh Flat': 'o', 'RTP +fixed': 'v', 'RTP +capacity': 's'}

    f, ax = plt.subplots()
    # plt.tick_params(
    #     axis='both',  # changes apply to the x-axis
    #     which='both',  # both major and minor ticks are affected
    #     bottom=False,  # ticks along the bottom edge are off
    #     top=False,  # ticks along the top edge are off
    #     labelbottom=False)  # labels along the bottom edge are off
    plt.axis('off')

    fig = (
        so.Plot(tech_cap_df, x='techs', y='energy_cap', color='Constraint', marker='Price scenario')
            .add(so.Dot(pointsize=7, edgecolor="gray"), so.Dodge(), so.Jitter(0.2))
            .scale(marker=so.Nominal(marker_dict))
            .label(x='', y='Capacity [kW]', title=title)
    )
    fig.save('{}.pdf'.format(title), dpi=600, bbox_inches="tight")
    fig.show()

#
def grouped_bar_plots(tech_df: pd.DataFrame, x_var, y_var, grouping, title, bbox=(0.9, 0.9)):
    sns.set_theme(style="whitegrid")
    sns.set(font_scale=2)
    g = sns.catplot(data=tech_df, kind='bar', x=x_var, y=y_var, hue=grouping,
                    palette="dark", alpha=.6, height=6)
    g.set_axis_labels('', title)
    # g.legend.set_title("")
    sns.move_legend(g, "upper right", bbox_to_anchor=bbox)
    # plt.figure(figsize=(18, 14))
    # plt.ylim(top=210)
    plt.tight_layout()
    fig = plt.gcf()
    fig.set_size_inches(14, 10)
    fig.savefig('{}.pdf'.format(title), bbox_inches="tight")
    plt.show()


tech_sel_el = [' Solar PV', 'Battery']
tech_sel_heat = ['Enhanced renovation', 'Improved renovation', 'Air source HP']
tech_sel_water_cooking = ['Electric boiler', ' Electric hob']
tech_sel_ren = ['Enhanced renovation', 'Improved renovation']
tech_sel_transport = ['car_ice', 'car_ev']
tech_sel_cooking = ['gas_hob', 'electric_hob']

el_techs = techs_df.loc[(techs_df['techs'].isin(tech_sel_el)) & (techs_df['locs'] != 'EG')].sort_values(
    ['techs', 'Constraint', 'Price scenario'], ascending=False)
# print(el_techs)

# for price_scen in techs_df['Price scenario'].unique():
#     for constraint in techs_df['Constraint'].unique():
#         for loc in techs_df['locs'].unique():
#             if not loc == 'EG':
#                 # print(price_scen)
#                 # print(constraint)
#                 # print(techs_df.loc[techs_df['Constraint'] == constraint])
#                 # print(techs_df.loc[(techs_df['Price scenario'] == price_scen)
#                 #                        & (techs_df['Constraint'] == constraint) & (techs_df['locs'] == loc)
#                 #                        & (techs_df['techs'] == 'Improved renovation'),
#                 #                        'energy_cap'])
#                 imp_ren = float(techs_df.loc[(techs_df['Price scenario'] == price_scen)
#                                        & (techs_df['Constraint'] == constraint) & (techs_df['locs'] == loc)
#                                        & (techs_df['techs'] == 'Improved renovation'),
#                                        'energy_cap'])
#                 exist_ren = float(techs_df.loc[(techs_df['Price scenario'] == price_scen)
#                                        & (techs_df['Constraint'] == constraint) & (techs_df['locs'] == loc)
#                                        & (techs_df['techs'] == 'Existing renovation'),
#                                        'energy_cap'])
#                 renovation_degree = imp_ren/(imp_ren + exist_ren)
#                 new_row = pd.DataFrame({'Price scenario': price_scen, 'Constraint': constraint, 'locs': loc,
#                                        'techs': 'Renovation degree', 'energy_cap': ["{:.3f}".format(renovation_degree)]})
#                 # print(new_row)
#                 techs_df = pd.concat([techs_df, new_row])

renovation_techs = techs_df.loc[(techs_df['techs'].isin(tech_sel_ren)) & (techs_df['locs'] != 'EG')].sort_values(
    ['Price scenario', 'Constraint', 'techs'], ascending=False)
heating_techs = techs_df.loc[(techs_df['techs'].isin(tech_sel_heat)) & (techs_df['locs'] != 'EG')].sort_values(
    ['Price scenario', 'Constraint', 'techs'], ascending=False)
water_cooking_techs = techs_df.loc[(techs_df['techs'].isin(tech_sel_water_cooking)) & (techs_df['locs'] != 'EG')].sort_values(
    ['Price scenario', 'Constraint', 'techs'], ascending=False)
print(water_cooking_techs)
# print(heating_techs.loc[heating_techs['techs']=='Renovation degree'])
evs = techs_df.loc[(techs_df['techs'] == 'car_ev') & (techs_df['locs'] != 'EG')].sort_values(
    ['Constraint', 'Price scenario'], ascending=False)
cooking_techs = techs_df.loc[(techs_df['techs'].isin(tech_sel_cooking)) & (techs_df['locs'] != 'EG')].sort_values(
    ['Constraint', 'Price scenario', 'techs'], ascending=False)
evs['energy_cap'].loc[evs['energy_cap'] < 2] = 0
evs['energy_cap'].loc[evs['energy_cap'] > 2] = 1
evs = evs.groupby(by=['Price scenario', 'Constraint']).sum().sort_values(
    ['Constraint', 'Price scenario'], ascending=False)
# print(evs)
# capacities_scatter(water_cooking_techs, 'El. water heating and cooking')


# capacities_scatter(heating_techs, 'Space heating technologies')
# capacities_scatter(heating_techs, 'Space & water heating electrification')
# capacities_scatter(renovation_techs, 'Renovation deployment')
# capacities_scatter(el_techs, 'Solar & storage deployment')
# capacities_scatter(transport_techs, 'vehicle deployment')
# capacities_scatter(cooking_techs, 'cooking deployment')
# grouped_bar_plots(evs, 'Constraint', 'energy_cap', 'Price scenario', "# of EVs", bbox=(0.38, 0.91))

def line_chart(df: pd.DataFrame, title, y_value='Cost', type=None):
    fig, ax = plt.subplots()
    if type:
        p = sns.lineplot(data=df, x='Constraint', y=y_value, hue='Price scenario', style=type)
    else:
        p = sns.lineplot(data=df, x='Constraint', y=y_value, hue='Price scenario')
    # , style='Cost type'
    sns.move_legend(p, "upper middle", bbox_to_anchor=(1, 1.1))
    # plt.ylim(top=300000)
    # ax.legend().set_title('')
    plt.ylabel(title)
    plt.xlabel('')
    plt.gca().yaxis.grid(True)
    plt.show()

def plot_clustered_stacked_sns(dfall, title="Investment and variable costs", bbox=(1.0, 1.01)):
    # c = ["blue", "purple", "red", "green", "pink"]
    sns.set(font_scale=2)
    # sns.set_theme(style="facetgrid")
    for i, g in enumerate(dfall.groupby("variable")):
        print(i, g)
        ax = sns.barplot(data=g[1],
                         x="index",
                         y="vcs",
                         hue="Price scenario",
                         # color=c[i],
                         # zorder=-i,  # so first bars stay on top
                         edgecolor="k",
                         palette="dark",
                         # width = 0.9,
                         alpha=.6)
        for bar in ax.patches:
            # bar.set_width(0.9)
            if g[0] == "Investments":
                bar.set_hatch('.')

    plt.ylabel("Cost  [1,000 Euro]")
    plt.xlabel('')
    h, l = ax.get_legend_handles_labels()
    labels = ["kWh Flat", "kWh ToU", "RTP +fixed", "RTP +capacity"]
    ax.legend(h[4:], labels, title="Price scenario")
    # ax.legend_.remove()  # remove the redundant legends
    sns.move_legend(ax, "upper left", bbox_to_anchor=bbox)
    fig = plt.gcf()
    fig.set_size_inches(14, 10)
    fig.savefig('{}.pdf'.format(title), bbox_inches="tight")
    plt.show()

cost_df['Cost'] = cost_df['Cost']/1000
print(cost_df)
inv_costs = cost_df.loc[cost_df['Cost type'].isin(['Capital costs'])]
inv_cost_flat = inv_costs.loc[inv_costs['Price scenario'].isin(['kWh Flat'])]['Cost'].tolist()
inv_cost_tou = inv_costs.loc[inv_costs['Price scenario'].isin(['kWh ToU'])]['Cost'].tolist()
inv_cost_rtfix = inv_costs.loc[inv_costs['Price scenario'].isin(['RTP +fixed'])]['Cost'].tolist()
inv_cost_rtcap = inv_costs.loc[inv_costs['Price scenario'].isin(['RTP +capacity'])]['Cost'].tolist()
var_costs = cost_df.loc[cost_df['Cost type'].isin(['Variable costs'])]
var_cost_flat = var_costs.loc[var_costs['Price scenario'].isin(['kWh Flat'])]['Cost'].tolist()
var_cost_tou = var_costs.loc[var_costs['Price scenario'].isin(['kWh ToU'])]['Cost'].tolist()
var_cost_rtfix = var_costs.loc[var_costs['Price scenario'].isin(['RTP +fixed'])]['Cost'].tolist()
var_cost_rtcap = var_costs.loc[var_costs['Price scenario'].isin(['RTP +capacity'])]['Cost'].tolist()
total_costs = cost_df.loc[(cost_df['Cost type'].isin(['Total cost']))]
kWh_flat = pd.DataFrame(data={"Investments": inv_cost_flat, "Variable": var_cost_flat},
                        index = ["No inv.", "No constr.", "Carb10", "Carb02"])
kWh_ToU = pd.DataFrame(data={"Investments": inv_cost_tou, "Variable": var_cost_tou},
                        index = ["No inv.", "No constr.", "Carb10", "Carb02"])
rtfix = pd.DataFrame(data={"Investments": inv_cost_rtfix, "Variable": var_cost_rtfix},
                        index = ["No inv.", "No constr.", "Carb10", "Carb02"])
rtcap = pd.DataFrame(data={"Investments": inv_cost_rtcap, "Variable": var_cost_rtcap},
                        index = ["No inv.", "No constr.", "Carb10", "Carb02"])


kWh_flat["Price scenario"] = "kWh Flat"
kWh_ToU["Price scenario"] = "kWh ToU"
rtfix["Price scenario"] = "RTP +fixed"
rtcap["Price scenario"] = "RTP +capacity"
dfall = pd.concat([pd.melt(i.reset_index(),
                           id_vars=["Price scenario", "index"]) # transform in tidy format each df
                   for i in [kWh_flat, kWh_ToU, rtfix, rtcap]],
                   ignore_index=True)
dfall.set_index(["Price scenario", "index", "variable"], inplace=True)
dfall["vcs"] = dfall.groupby(level=["Price scenario", "index"]).cumsum()
dfall.reset_index(inplace=True)
# unique_cat = ["kWh Flat", "kWh ToU", "RTP +fixed", "RTP +capacity"]
# dfall["Price scenario"] = pd.Categorical(dfall['Price scenario'], categories = unique_cat)
plot_clustered_stacked_sns(dfall)

# print(kWh_flat)
cost_spreads = cost_df.loc[cost_df['Cost type'] == 'Cost spread']
diesel = cost_df.loc[cost_df['Cost type'] == 'Diesel']
# plot_clustered_stacked([kWh_flat, kWh_ToU, rtfix, rtcap],
#                        labels=['kWh Flat', 'kWh ToU', 'RTP +fixed', 'RTP +capacity'],
#                        cmap=plt.cm.viridis)
# print(total_costs)
# grouped_bar_plots(total_costs, 'Constraint', 'Cost', 'Price scenario', 'Total cost [1000 Euro]', bbox=(0.65, 0.95))
# grouped_bar_plots(inv_costs, 'Constraint', 'Cost', 'Price scenario', 'Investment cost [1000 Euro]', bbox=(0.47, 0.93))
# grouped_bar_plots(var_costs, 'Constraint', 'Cost', 'Price scenario', 'Variable cost [1000 Euro]', bbox=(0.9, 0.92))
el_peak = network_df.loc[network_df['Indicator'].isin(['Load peak'])]
feed_peak = network_df.loc[network_df['Indicator'].isin(['Feed-in peak'])]
# print(feed_peak)
gas_peak = network_df.loc[network_df['Indicator'].isin(['Gas peak'])]
el_avg = network_df.loc[network_df['Indicator'].isin(['Load avg'])]
feed_avg = network_df.loc[network_df['Indicator'].isin(['PV gen.'])]
# print(feed_avg)
gas_avg = network_df.loc[network_df['Indicator'].isin(['Gas avg'])]


# print(el_peak)
# grouped_bar_plots(el_peak, 'Constraint', 'Value', 'Price scenario', 'El. load peak [kW]', bbox=(0.4, 0.9))
# grouped_bar_plots(feed_peak, 'Constraint', 'Value', 'Price scenario', 'El. feed-in peak [kW]', bbox=(0.48, 0.66))
# grouped_bar_plots(gas_peak, 'Constraint', 'Value', 'Price scenario', 'Gas peak [kW]', bbox=(0.9, 0.9))
# grouped_bar_plots(el_avg, 'Constraint', 'Value', 'Price scenario', 'El. grid load avg [kW]', bbox=(0.38, 0.9))
# grouped_bar_plots(feed_avg, 'Constraint', 'Value', 'Price scenario', 'Total feed-in [1000 kWh]', bbox=(0.44, 0.44))
# grouped_bar_plots(gas_avg, 'Constraint', 'Value', 'Price scenario', 'Gas avg [kW]', bbox=(0.9, 0.9))
#

def capacities_bar(df, title):
    f, ax = plt.subplots()
    plt.tick_params(
        axis='both',  # changes apply to the x-axis
        which='both',  # both major and minor ticks are affected
        bottom=False,  # ticks along the bottom edge are off
        top=False,  # ticks along the top edge are off
        labelbottom=False)  # labels along the bottom edge are off
    plt.axis('off')
    p = so.Plot(df, x="Constraint", y="Total use", color='Carrier').add(so.Bar(), so.Stack()) \
        .label(x='', y=title).scale(color="rocket_r")
    # .layout(engine='tight').on(f).plot()
    plt.ticklabel_format(style='plain', axis='y', useOffset=False)
    # sns.color_palette("hls", 3)
    sns.set(font_scale=2)
    ax = plt.gca()
    # plt.legend(loc="upper right", bbox_to_anchor=(1, 1))
    # p.set_size_inches(14, 10)
    p.save('test.pdf', dpi=600, bbox_inches="tight")
    f.savefig('Consumption.pdf')
    fig = plt.gcf()
    fig.set_size_inches(14, 10)
    fig.savefig('{}.pdf'.format(title), bbox_inches="tight")
    p.show()


carrier_sel = ['El. total', 'Gas', 'Diesel']
resource_df = resource_df.loc[resource_df['Carrier'].isin(carrier_sel)].groupby(
    by=['Constraint', 'Carrier']).mean().sort_values(['Constraint'], ascending=False)
resource_df['Total use'] = resource_df['Total use'].div(1000)
# print(resource_df)

# capacities_bar(resource_df, 'Total consumption [MWh]')


def production_area(tech_sel, prod_df, title):
    # list of columns to group for stacked area graph of production of all households
    col_group_list = ['timesteps', 'techs', 'carriers']
    tech_prod = prod_df.loc[(prod_df['techs'].isin(tech_sel)) & (prod_df['locs'] != 'EG')]
    tech_prod_sum = tech_prod.groupby(col_group_list).sum().reset_index().drop('carriers', axis=1)
    tick_labels = [timestep for timestep in tech_prod_sum.iloc[::24, :]['timesteps']]
    p = so.Plot(tech_prod_sum, 'timesteps', 'carrier_prod', color='techs').add(so.Area(alpha=0.7), so.Stack())
    p.scale(x=so.Continuous().tick(at=tick_labels))
    p.show()

# for title, tech_sel in tech_selections.items():
#     tech_cap_df = cap_df.loc[(cap_df['techs'].isin(tech_sel)) & (cap_df['locs'] != 'EG')]
#     capacities_bar(tech_sel, cap_df, title)
#     # capacities_scatter(tech_sel, cap_df, title)
#     # production_area(tech_sel, prod_df, title)
#
