
import calliope
import create_input
import run
from datetime import datetime
from argparse import ArgumentParser
# for parallelization, if necessary:
from joblib import Parallel, delayed

def handle_cmdline():
    parser = ArgumentParser()

    parser.add_argument(
        '-n', '--noinvest',
        action='store_true',
        help='Choose for no investment run.')

    parser.add_argument(
        '-c', '--carbon',
        default='100',
        choices=['1', '2', '5', '10', '20', '30', '40', '50', '60', '70', '80', '90', '100'],
        help='Carbon budget. Only 1, 2, 5 and multiples of 10 between 10 and 100 allowed.')

    parser.add_argument(
        '-t', '--timeres',
        default='1',
        choices=['1','2','3','6','12','24','144'],
        help='Time resolution. Choose between 1, 2, 3, 6, 12, 24 and 144 hours.')

    parser.add_argument(
        '-e', '--elprice',
        default='x2',
        choices=['x1', 'x2', 'x4'],
        help='Electricity price multiplier. Choose between x1, x2 and x4.')

    return parser.parse_args()

args = handle_cmdline()

no_investment_run = args.noinvest
path_to_model_yaml = 'model.yaml'
n_cpus = 4 # number of cpus available for parallelization

# , "tou_tou", "fixed_tou", "fixed_rtp", "cap_flat", "cap_tou" , 'cap_rtp', 'flat_flat', "fixed_rtp"
network_elec_prices = ['tou_tou', 'cap_rtp', 'flat_flat', "fixed_rtp"]

# electricity price multiplier
# "x1", , "x4"
el_price_fac = args.elprice

carbon_budget = args.carbon

time_res = args.timeres

def get_scenario(scen_name):
    scen_parts = scen_name.rsplit("_")
    net_tariff = scen_parts[0]
    elec_price = scen_parts[1]
    return net_tariff, elec_price

def run_model(price_scen, carbon_budget, el_price_fac, no_investment):
    if no_investment:
        scenarios_string = f'res_{time_res}h,no_investments'
    else:
        scenarios_string = f'car_ev_costs,carbon_budget_{carbon_budget},res_{time_res}h'

    net_tariff, elec_price = get_scenario(price_scen)
    scenarios_string += f',elec_{elec_price}'
    if price_scen == "tou_tou":
        # if both network and electricity are ToU we use a different data set
        scenarios_string = scenarios_string.replace('elec_tou', 'network_elec_tou')
    elif price_scen == 'flat_flat':
        # if both network and electricity are flat we use a different data set
        scenarios_string = scenarios_string.replace('elec_flat', 'network_elec_flat')
    if not el_price_fac == "x1":
        scenarios_string += f'_{el_price_fac}'
    if 'cap' in price_scen:
        # if network tariff is capacity based we add an extra price for capacity
        scenarios_string += ',network_cap'
    calliope.set_log_verbosity("info", include_solver_output=True, capture_warnings=True)
    model = calliope.Model(path_to_model_yaml, scenario=scenarios_string)
    model = run.update_constraint_sets(model)
    model.run(build_only=True)
    scenarios_string = scenarios_string.replace(',', '_').replace('car_ev_costs_', '').replace('res_1h_', '')
    if 'network' not in scenarios_string:
        scenarios_string += '_network_' + net_tariff
    timestamp_format = "{:%Y_%m_%d_%H-%M}"
    timestamp = timestamp_format.format(datetime.now())
    scenarios_string += '_' + timestamp
    run.add_eurocalliope_constraints(model)
    new_model = model.backend.rerun()
    new_model.plot.summary(to_file=f"{scenarios_string}.html")
    new_model.to_netcdf(f"{scenarios_string}_netcdf")
    new_model.to_csv(f"{scenarios_string}_csv")
    # caps = new_model.get_formatted_array("energy_cap").to_pandas()

Parallel(n_jobs=max(n_cpus, len(network_elec_prices)))\
        (delayed(run_model)(price_sen, carbon_budget, el_price_fac, no_investment_run)
         for price_sen in network_elec_prices)

