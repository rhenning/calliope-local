import yaml

carbon_unconstrained = 203000

budgets = ["100", "90", "80", "70", "60", "50", "40", "30", "20", "10"]

decimal_factors = {"100": 1, "90": 0.9, "80": 0.8, "70": 0.7, "60": 0.6, "50": 0.5,
                   "40": 0.4, "30": 0.3, "20":0.2, "10": 0.1}

carbon_budgets = {budget: carbon_unconstrained * decimal_factors[budget] for budget in budgets}

print(carbon_budgets)